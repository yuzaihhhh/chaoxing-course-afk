# -*- coding:utf-8 -*-

import json
import os
import random
import re
import time

import requests


class Student:

    def _check_user_folder(self):
        '''
        check "userdat" folder is it here
        '''
        self._userdat_folder = './userdat'
        if os.path.isdir(self._userdat_folder):
            return 1
        else:
            return 0

    def _read_cookies(self):
        '''
        read user cookies can't found just exit 
        '''
        cookiesdat_fle = '/cookies.dat'
        # check file is they here
        if os.path.isfile(''.join((self._userdat_folder, cookiesdat_fle, ))):
            # if in here just read cookies
            with open(''.join((self._userdat_folder, cookiesdat_fle, )), 'r') as target:
                self._cookies = json.load(target,)
        else:
            input('can not find the "cookies.dat" file\npass enter to contiue...')
            exit(0)

    def _read_lgin_dat(self):
        '''
        read user login data
        '''
        logindat_fle = '/login.dat'
        # check file is they here
        if os.path.isfile(''.join((self._userdat_folder, logindat_fle, ))):
            with open(''.join((self._userdat_folder, logindat_fle, ))) as target:
                self._lgin_dat = json.load(target)
        else:
            input('can not find the "login.dat" file\npass enter to contiue...')
            exit(0)

    def __init__(self):
        # "self._userdat_folder" param is in the "_read_cookies" method
        self._req_session = requests.Session()
        self._headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'}
        #
        if self._check_user_folder() == 1:
            # "self._cookies" is in self._read_lgin_dat()
            self._read_cookies()
            # "self._lgin_dat" is in self._read_cookies()
            self._read_lgin_dat()
        else:
            input('can not found the "userdat" folder\npass enter to continue')
            exit(0)

    def _get_auth_code(self):
        req = self._req_session.get(url='http://passport2.chaoxing.com/num/code?%s' % int(time.time()*1000),
                                    headers=self._headers,
                                    )
        with open('draft/authcode.jpeg', 'wb+') as target:
            target.write(req.content)
            self._lgin_dat['numcode'] = input(
                'Please open the "./authcode.jpeg" an input the auth code: ')

    def _lgin_chaoxing(self):
        '''
        post login info to login page to compile logging
        '''
        self._get_auth_code()
        req = self._req_session.post(url='http://passport2.chaoxing.com/login?refer=http://mooc1-1.chaoxing.com/visit/courses',
                                     headers=self._headers,
                                     data=self._lgin_dat)
        print(req.text)

    def _brush_classes_page(self):
        req = self._req_session.get(url='https://mooc1-1.chaoxing.com/mycourse/studentcourse?courseId=201076871&clazzid=3292931&ut=s&enc=928ca680a2ddaa1b364e67bb52eb836d',
                                    headers=self._headers,
                                    cookies=self._cookies)
        req = self._req_session.get(url='https://fystat-ans.chaoxing.com/log/setlog?uid=57774604&courseId=201076871&classId=3292931&encode=c95cb51ef73091de199353d1dd7646ca',
                                    headers=self._headers,
                                    cookies=self._cookies)
        print(''.join(('\r', req.text)))

    def _show_class_progress_of_access(self):
        # \t{13}<td >\n\t{13}(.+?)\n\t{11}</td>
        #<td >\n\s{104}(.+?)\n\s{88}</td>
        # https://mooc1-1.chaoxing.com/studyprogress?courseId=201076871&classId=3292931&ut=s&enc=928ca680a2ddaa1b364e67bb52eb836d

        req = self._req_session.get(url='https://mooc1-1.chaoxing.com/studyprogress?courseId=201076871&classId=3292931&ut=s&enc=928ca680a2ddaa1b364e67bb52eb836d',
                                    headers=self._headers,
                                    cookies=self._cookies)
        reobj = re.compile(r'<td >\n\s{104}(.*?)\n\s{88}</td>')
        re_result = reobj.findall(req.text)
        print(re_result)

    def req_chaoxing_webpage(self):
        req = self._req_session.get(url='http://mooc1-1.chaoxing.com/visit/courses',
                                    headers=self._headers,
                                    cookies=self._cookies
                                    )
        if '您确定要退出此课程？退课后学习记录将无法恢复！' in req.text:
            req_times = 0
            while req_times<30:
                self._brush_classes_page()
                req_times += 1
                print(''.join(('\r', '已经执行了 %s 次'.rjust(75, ' ') % req_times, )), end='')
                time.sleep(random.randint(1, 300))
            #self._show_class_progress_of_access()
        else:
            self._get_auth_code()
            # self._lgin_chaoxing()


def main():
    std = Student()
    std.req_chaoxing_webpage()


if __name__ == '__main__':
    main()
